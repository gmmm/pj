function FindProxyForURL(url, host) {
  var porxyindex = 0;
 // var porxyindex = 16;   
  
  var proxy_direct = "DIRECT;";
  var proxy_yes = [
"SOCKS5 8.210.5.23:8080;socks5 139.59.7.194:1080;"//0 
 ,"SOCKS5 95.217.4.59:1081;47.254.129.46:9000;47.110.49.177:1080;115.205.66.220:1080;"//1
 ,"socks5 179.43.176.193:10084;SOCKS5 ;"//2 
,"SOCKS5 119.23.64.28:8080;SOCKS5 117.102.84.142:443;"//3 
,"HTTPS server.gomcomm.com:443"//4 hk
,"HTTPS sg-3.gododdy.xyz:443;HTTPS node.de.gododdy.xyz:443;"//5 
  ,"HTTPS node.in.gododdy.xyz:443;"//6
  ,"HTTPS uk.cn-cloudflare.com:443;"//7
  ,"HTTPS ca.cn-cloudflare.com:443;"//8
  ,"HTTPS de.cn-cloudflare.com:443;"//9
  ,"HTTPS sg2.cn-cloudflare.com:443;"//10    
  ,"HTTPS jp.cn-cloudflare.com:443;"//11   
  ,"https fr1.beforrent.com:9152;"  //12  
  ,"HTTPS usa4.cn-cloudflare.com:443" //13
  ,"HTTPS hgc.ghelper.net:443;"//14
  ,"HTTPS sg2.ghelper.net:443;"//15 
  ,"HTTPS de.ghelper.net:443;"//16 
  ,"HTTPS tw.ghelper.net:443;"//17 
  ,"HTTPS ru2.ghelper.net:443;"//18 
  
  ,"HTTPS www.ghelper.net:3389;"//19
  ,"HTTPS london.best-proxy.com:443;"//20 ge
  ,"HTTPS host9new.is-a-soxfan.org:18443;"//21 Total US
  ,"HTTPS uup.co.54honxd.club:443;"//22 
  ,"HTTPS sg11.tcdn.me:443;"//23 
  ,"HTTPS 16minsecond.selfip.biz:18443;"//24 
  ,"HTTPS sv12day.dontexist.org:18443;"//25 
  ,"https  neo.vpnmama.com:443;;"//26
  ,"HTTPS hkt5.ghelper.net:443;"//27 
  ,"HTTPS www.copyplay.net:3389;"//28 
  ,"HTTPS oo.54honxd.club:443;"//29 
  ,"HTTPS hkbn2.ghelper.net:443"//30 
  ,"HTTPS uup.co.54honxd.club:443"//31
  ,"HTTPS shop.cdn-aliyun.com:443"//32
  ,"HTTPS us02.ghelper.net:443;"//33
  ,"HTTPS bv1.innlym.me:443;"//34
  ,"HTTPS jp4.ghelper.net:443;"//35
  ,"HTTPS hkcm.ghelper.net:443;"//36
  ,"HTTPS hkt6.ghelper.net:3389;"//37
  ,"HTTPS www.tcpbbr.net:1443;"//38
  ,"HTTPS www.51netflix.com:443;"//39
  ,"HTTPS wtt.21t.xyz:443;ru.onhop.net:443;"//40
  ,"HTTPS hkwtt.ghelper.net:443;"//41
  ,"HTTPS www.pickdown.net:443;"//42
  ,"HTTPS hgc.tcpbbr.net:443;"//43
  ,"HTTPS fr.tcpbbr.net:443;"//44
  ,"HTTPS us.tcpbbr.net:443;"//45
  ,"HTTPS hkt.21t.xyz:443;"//
  ,"HTTPS us2.tcpbbr.net:443:"//
  ,"HTTPS 54.36.117.30:3128;"// 48
  ,"HTTPS www.gunan.bid:9001"//49
  ,"HTTPS uk.jiuyoujiu999.top:4033"//50
  ,"HTTPS 61.228.142.75:80;"//51
  ,"HTTPS ruasdeisdfe.cdn-aliyun.com:443"//52
  ,"HTTPS us2vip2sfjgrgerg.cdn-aliyun.com:443"//53
  ,"HTTPS los5asdsfesfsffw3.cdn-aliyun.com:443"//54
  ,"HTTPS sgfdisfesdgd.cdn-aliyun.com:443"//55
  ,"HTTPS sg01jsfewfie.cdn-aliyun.com:443"//56
  ,"HTTPS dc2shark09328.cdn-aliyun.com:443"//57
  ,"HTTPS cdf240dcca462a8b6912827e34ae1eb3.do-de-fra1-01-v6612xny.adguard.io:443;HTTPS cdf240dcca462a8b6912827e34ae1eb3.gc-it-mil-02-jxaukmlk.adguard.io:443;"//58
  ,"HTTPS cdf240dcca462a8b6912827e34ae1eb3.gc-pl-waw-01-9splgfzs.adguard.io:443;HTTPS cdf240dcca462a8b6912827e34ae1eb3.gc-ua-kyv-02-00yzh86s.adguard.io:443;"//59
 ,"HTTPS cdf240dcca462a8b6912827e34ae1eb3.gc-es-mad-03-f1vu2seq.adguard.io:443;HTTPS cdf240dcca462a8b6912827e34ae1eb3.gc-us-nyc-01-2gzkth0v.adguard.io:443;HTTPS cdf240dcca462a8b6912827e34ae1eb3.gc-nl-ams-01-lx6v4ksk.adguard.io:443;"//60
 ,"HTTPS d2d92a86a6feeb5cfa4d3f48bfcfd873.do-gb-lon1-02-o185ruhs.adguard.io:443;HTTPS d2d92a86a6feeb5cfa4d3f48bfcfd873.crnv-fi-hel-01-opfn482v.adguard.io:443;HTTPS d2d92a86a6feeb5cfa4d3f48bfcfd873.gc-lu-lux-02-3firpyq3.adguard.io:443;"//61
  ];

  var proxy_yeslist = ["45.32.164.128", "*google*", "*goo.gl*", "*gmail*", "*youtube*", "*ytimg.com*",
    "*chrome.com*", "*android.com*", "*ggpht.com*", "*blogger.com*", "*wordpress.com*", "*facebook.com*",
    "*twitter.com*", "*twimg.com*", "*ip8.com*", "*tumblr.com*", "*instagram.com*", "*blogspot.com*",
    "*blogblog.com*", "dist.github.com*", "*.tensorflow.org*", "*shadowsocks.org*"
  ];
  var proxy_directlist = [
    "*baidu.com*", "*qq.com*","*qqmail.com*", "ip111.cn", "*csdn.net*", "*proxyscrape.com*","*jb51.net*","ifeng.com",
    "*toutiao.com*", "*tianya.cn*", "*dichvusocks.us*", "*ggithub.com*","*gitlab.com*","*guancha.cn*","*360.cn*"
    ,"*pangzi.ca*","*gttv.tv*","*huaweicloud*",
    "*githubusercontent*", "*cnblogs.com*","*tianyaui.com*"
    ,"*pstatp.com*","*byteimg*","*.huanqiu.*","*jianshu.com*","*jd.com*"
  ];
  
  for (var i = 0; i <
    proxy_directlist.length; i++) {
    if (shExpMatch(host, proxy_directlist[i]))
      return proxy_direct
  }
  if (shExpMatch(host, "10.[0-9]+.[0-9]+.[0-9]+")) {
        return proxy_direct;
    }
    if (shExpMatch(host, "172.[0-9]+.[0-9]+.[0-9]+")) {
        return DIRECT;
    }
    if (shExpMatch(host, "192.168.[0-9]+.[0-9]+")) {
        return proxy_direct;
    }
    if (shExpMatch(host, "127.[0-9]+.[0-9]+.[0-9]+")) {
        return proxy_direct;
    }
    if (shExpMatch(host, "localhost")) {
        return proxy_direct;
    }
  //porxyindex =59;
  return proxy_yes[porxyindex]
}