function FindProxyForURL(url, host) {
              var servers = '{"Moscow, Russian Federation":["185.159.82.172:11084","185.130.104.137:11084","185.130.104.139:11084"],"Denver, Colorado, USA":["23.237.16.26:11084","23.237.16.27:11084"],"Netherlands, Amsterdam":["185.130.105.66:11084","185.209.162.141:11084","185.162.131.46:11084"],"Vienna, Austria":["50.7.184.98:11084","50.7.184.99:11084"],"London, United Kingdom":["50.7.154.98:11084","50.7.154.99:11084"],"Miami, Florida, USA":["192.240.98.162:11084","192.240.98.163:11084"],"Chicago, Illinois, USA":["23.237.128.50:11084","23.237.128.51:11084"],"California, USA":["185.180.198.66:11084","162.244.32.43:11084","104.193.252.169:11084"],"New York, USA":["185.180.197.65:11084","204.155.30.126:11084","185.180.197.93:11084"],"Warsaw, Mazovia, Poland":["188.116.27.87:10084"],"Frankfurt am Main, Hesse, Germany":["5.61.40.159:10084"],"Tallinn, Harjumaa, Estonia":["37.252.5.96:10084"],"Zurich, Switzerland":["179.43.176.193:10084"],"Kyiv, Ukraine":["130.0.232.227:10084"],"Stockholm, Sweden":["37.252.8.185:10084"]}';
              var whiteList = undefined;

              if(servers.indexOf(host) > -1 || dnsDomainIs(host, 'ip-api.com')
                  || dnsDomainIs(host, 'statapi.extenbalanc.org')
                  || dnsDomainIs(host, 'errorcatcher.ininja.org')
                ) {
                return 'system';
              }
              if (isPlainHostName(host) || dnsDomainLevels(host) == 0 ||
                  shExpMatch(host, "*.local") || shExpMatch(host, "localhost") ||
                  isInNet(dnsResolve(host), "10.0.0.0", "255.0.0.0") ||
                  isInNet(dnsResolve(host), "172.16.0.0",  "255.240.0.0") ||
                  isInNet(dnsResolve(host), "192.168.0.0",  "255.255.0.0") ||
                  isInNet(dnsResolve(host), "127.0.0.0", "255.255.255.0")) {
                return 'system';
              }

              if(whiteList && whiteList.length > 0 && whiteList.indexOf(host.replace(/^www./,'')) == -1) {
                return 'system';
              }
              return 'socks5 37.252.5.96:10084';
            }